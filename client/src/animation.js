import THREE from 'three';

export function animation(updates) {

  var camera, scene, renderer;
  var geometry, material, mesh;


  init();
  updates.subscribe(render);

  function init() {

    camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 1, 10000);
    camera.position.z = 10;

    scene = new THREE.Scene();

    var ambient = new THREE.AmbientLight( 0xDDDDDD );
    scene.add( ambient );

    // var loader = new THREE.JSONLoader();
    // loader.load('model/amethyst-star-high-resolution.json', (geometry, materials) => {
    //   var material = new THREE.MeshFaceMaterial( materials );
    //   mesh = new THREE.Mesh( geometry, material );
    //   scene.add( mesh );});

    var loader = new THREE.ObjectLoader();
    loader.load('model/amethyst-star-high-resolution.json',function ( obj ) {
      mesh = obj;
      scene.add( obj );
      mesh.rotateY(Math.PI * 0.75);
      renderer.render(scene, camera);
    });
    renderer = new THREE.WebGLRenderer({canvas: window.document.getElementById('animation')});
    renderer.setSize(window.innerWidth, window.innerHeight);
  }

  function render(event) {
    if(mesh){
      mesh.quaternion.slerp(updateDeviceMove(event), 0.5);
    }
    renderer.render(scene, camera);
  }


  let updateDeviceMove = function (ev) {
    var alpha, beta, gamma, orient = 0;
    var deviceQuat = new THREE.Quaternion();

    alpha  = THREE.Math.degToRad( ev.alpha || 0 ); // Z
    beta   = THREE.Math.degToRad( ev.beta  || 0 ); // X'
    gamma  = THREE.Math.degToRad( ev.gamma || 0 ); // Y''

    deviceQuat = createQuaternion( alpha, beta, gamma, orient );
    return deviceQuat;
  };

  var createQuaternion = function ( alpha, beta, gamma, screenOrientation ) {
    let finalQuaternion = new THREE.Quaternion();
    let deviceEuler = new THREE.Euler();
    let screenTransform = new THREE.Quaternion();
    let worldTransform = new THREE.Quaternion( - Math.sqrt(0.5), 0, 0, Math.sqrt(0.5) ); // - PI/2 around the x-axis
    var minusHalfAngle = 0;
    deviceEuler.set( beta, alpha, - gamma, 'YXZ' );
    finalQuaternion.setFromEuler( deviceEuler );
    minusHalfAngle = - screenOrientation / 2;
    screenTransform.set( 0, Math.sin( minusHalfAngle ), 0, Math.cos( minusHalfAngle ) );
    finalQuaternion.multiply( screenTransform );
    finalQuaternion.multiply( worldTransform );
    return finalQuaternion;

  };
}
