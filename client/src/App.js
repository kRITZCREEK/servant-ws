import React, { Component } from 'react';
import THREE from 'three';
import {animation} from './animation.js';
import io from 'socket.io-client';
import {Observable} from 'rx';

export default class App extends Component {

  constructor() {
    super();
    this.state = {
      socket: io()
    };
  }

  componentDidMount() {
    Observable.fromEvent(window, 'deviceorientation')
      .throttle(10)
      .filter(e => e.alpha && e.beta && e.gamma)
      .subscribe(e => {
        this.state.socket.emit('event', {alpha: e.alpha, beta: e.beta, gamma: e.gamma});
      });

    let updates = Observable.fromEvent(this.state.socket, 'event');
    animation(updates);
  }

  render() {
    return (
      <div>
        <canvas id="animation"></canvas>
      </div>
    );
  }
}
