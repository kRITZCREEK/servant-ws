{-# LANGUAGE DataKinds         #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators     #-}

module Main where

import           Data.Monoid              ((<>))
import           Network.EngineIO.Wai
import qualified Network.SocketIO         as SocketIO
import           Network.Wai
import           Network.Wai.Handler.Warp (run)
import           Servant
import           System.Environment


import           WS                       (eioServer)


type API = "socket.io" :> Raw
      :<|> Raw


api :: Proxy API
api = Proxy


server :: WaiMonad () -> Server API
server sHandler = socketIOHandler
    :<|> serveDirectory "public"
    where
      socketIOHandler = toWaiApplication sHandler


app :: WaiMonad () -> Application
app sHandler = serve api $ server sHandler


defaultPort :: Int
defaultPort = 3001

main :: IO ()
main = do
  port <- maybe defaultPort read <$> lookupEnv "PORT"
  sHandler <- SocketIO.initialize waiAPI eioServer
  putStrLn $ "Running on " <> show port
  run port $ app sHandler
