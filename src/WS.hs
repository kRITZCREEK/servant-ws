{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module WS (eioServer) where

import           Control.Monad (mzero)
import           Control.Monad.IO.Class (MonadIO, liftIO)
import           Control.Monad.State.Class (MonadState)
import           Data.Aeson
import qualified Network.SocketIO as SocketIO

data DOEvent =
  DOEvent {
   alpha :: Double
   , beta :: Double
   , gamma :: Double
  }

instance FromJSON DOEvent where
  parseJSON (Object o) =
    DOEvent <$>
     o .: "alpha" <*>
     o .: "beta" <*>
     o .: "gamma"
  parseJSON _ = mzero

instance ToJSON DOEvent where
  toJSON (DOEvent a b g) = object ["alpha" .= a, "beta" .= b, "gamma" .= g]

--------------------------------------------------------------------------------
eioServer :: (MonadState SocketIO.RoutingTable m, MonadIO m) => m ()
eioServer = do
  liftIO $ putStrLn "New Connect"
  SocketIO.on "event" $ \(ev :: DOEvent) -> do
    -- liftIO $ putStrLn "Success"
    SocketIO.emit "event" ev
    SocketIO.broadcast "event" ev

  SocketIO.appendDisconnectHandler $
    liftIO $ putStrLn "DC"
